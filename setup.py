from setuptools import setup, find_packages


with open("README.md") as f:
    long_description = f.read()

print(find_packages())

setup(
    name="brian",
    description="Static site generator",
    version="0.0.1",
    author="Joao Freitas",
    author_email="joaj.freitas@gmail.com",
    license="GPLv3",
    url="https://gitlab.com/joajfreitas/fcp-core",
    #download_url="https://gitlab.com/joajfreitas/fcp-core/-/archive/v0.32/fcp-core-v0.29.tar.gz",
    packages=find_packages(),
    entry_points={"console_scripts": ["brian = brian.__main__:main",],},
    install_requires=["click", "networkx", "bottle", "loguru", "pystache", "matplotlib", "pyyaml", "markdown", "pymdown-extensions", 'markdown-katex'],
    long_description=long_description,
    long_description_content_type="text/markdown"
)
