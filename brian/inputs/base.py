from pathlib import Path

class InputBase():
    def path_convert(self, cfg):
        src = cfg.input.relative_to(cfg.src)
        return Path(*src.parts[:-1]) / (src.stem + self.out_ft)

    def input(self, cfg):
        return "", {}
