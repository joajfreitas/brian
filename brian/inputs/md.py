# # Extensions
# https://facelessuser.github.io/pymdown-extensions/


import markdown
from .wikilinks import WikiLinkExtension
import pymdownx.superfences as superfences
import pymdownx.arithmatex as arithmatex
import pymdownx.emoji as emoji
#import caption


from .base import InputBase


class InputMarkdown(InputBase):
    def __init__(self):
        self.out_ft = ".html"
        pass

    def input(self, cfg):
        self.cfg = cfg
        self.cfg.output = self.path_convert(cfg)
        extensions = [
                "pymdownx.extra",
                "pymdownx.tasklist",
                'pymdownx.critic',
                'pymdownx.emoji',
                "pymdownx.inlinehilite",
                #'pymdownx.arithmatex',
                'pymdownx.tabbed',
                'pymdownx.snippets',
                'pymdownx.mark',
                'pymdownx.magiclink',
                'pymdownx.keys',
                'markdown.extensions.toc',
                'markdown.extensions.meta',
                'markdown_katex',
                WikiLinkExtension(base_url=str(cfg.out.absolute()) + "/" , end_url=".html"),
                #caption.captionExtension(stripTitle=False)
        ]

        extension_config = {
            "pymdownx.inlinehilite": {
                "custom_inline": [
                    {"name": "math", "class": "arithmatex", "format": arithmatex.inline_mathjax_format},
                ]
            },
            "pymdownx.superfences": {
                "custom_fences": [
                    {
                        "name": "math", 
                        "class": "arithmatex", 
                        "format": arithmatex.inline_mathjax_format
                    },
                    {
                        'name': 'mermaid',
                        'class': 'mermaid',
                        'format': superfences.fence_div_format
                    }
                ]
            },
            "pymdownx.emoji": {
                "options": {
                    "attributes": {
                        "height": "20px",
                        "width": "20px"
                        },
                    }
                }
            }

        with open(cfg.input) as f:
            txt = f.read()

        md = markdown.Markdown(extensions = extensions, extension_configs=extension_config)
        txt = md.convert(txt)

        return txt, md.Meta

        print(md.Meta)

        with open("theme/default.html") as f:
            theme = f.read()

        return render(theme, {**{"body":txt}, **md.Meta})


