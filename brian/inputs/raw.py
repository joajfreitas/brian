from .base import InputBase

class InputMarkdown(InputBase):
    def __init__(self):
        pass

    def input(self, cfg):
        with open(cfg["input"]) as f:
            txt = f.read()

        return markdown(txt)

