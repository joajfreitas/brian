from pathlib import Path
from  shutil import copy

from .base import OutputBase

class OutputRaw(OutputBase):
    def __init__(self):
        self.filetype = ""
        return

    def output(self, cfg, txt):
        self.filetype = cfg.input.suffix
        cfg.output = self.path_convert(cfg)
        
        cfg.output.parent.mkdir(exist_ok=True)
        copy(cfg.input, cfg.output)
