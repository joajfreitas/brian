from pathlib import Path

from .base import OutputBase

class OutputHTML(OutputBase):
    def __init__(self):
        self.filetype = ".html"
        return

    def output(self, cfg, txt):
        cfg.output = self.path_convert(cfg)
        
        cfg.output.parent.mkdir(parents=True, exist_ok=True)
        with cfg.output.open("w") as f:
            f.write(txt)
            
