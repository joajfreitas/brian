import os
import glob
from pathlib import Path
import re

import networkx as nx
import matplotlib.pyplot as plt

import yaml
import click
from loguru import logger
from bottle import static_file, route, run

from pystache import render

from .inputs import *
from .outputs import *

from .post import Input

input_mapping = {
        "md": InputMarkdown(),
        "png": InputNone(),
        "svg": InputNone(),
        "css": InputNone(),
}

io_mapping = {
        "md": "html",
        "png": "png",
        "svg": "svg",
        "css": "css",
}

template_mapping = ["md"]

output_mapping = {
        "html": OutputHTML(),
        "png": OutputRaw(),
        "svg": OutputRaw(),
        "css": OutputRaw()
}

input_none = InputNone()
output_none = OutputNone()
io_none = None


class PathConfig():
    def __init__(self, src=None, out=None, input=None, output=None):
        self.src = src
        self.out = out
        self.input = input
        self.output = output

    def copy(self):
        return PathConfig(self.src, self.out, self.input, self.output)

def get_source_files(src):
    return filter(lambda x : x.is_file(), src.rglob("*"))

@click.command("convert")
@click.argument("src_path", type=Path)
@click.argument("out_path", type=Path)
def convert(src_path, out_path):

    cfg = PathConfig(
            src = src_path, 
            out=out_path)
    
    with open(cfg.src / "config.yaml") as f:
        print(yaml.safe_load(f.read()))

    srcs = get_source_files(src_path) 

    inputs = []
    for src in srcs:
        cfg.input = src

        # last component ([-1]) and without the '.'
        extension = src.suffixes[-1][1:] 
        input_handler = input_mapping.get(extension, input_none)
        ins = input_handler.input(cfg)
        inputs.append(Input(ins, cfg.copy()))

    
    index_input = Input(("", {"theme": ["index"]}), 
            PathConfig(src_path, out_path, src_path / "index.md", "index.html"))
    inputs.append(index_input)
    
    slugs = []
    for ins in inputs:
        if not ins.ext == "md":
            continue

        print(ins.cfg.output)
        if str(ins.cfg.output) == "index.html":
            continue

        slugs.append({"slug": ins.slug, "path": ins.cfg.output})

    
    templateds = []

    G = nx.DiGraph()
    for src in srcs:
        G.add_node(src.stem)

    for ins in inputs:
        if not ins.ext in template_mapping:
            templateds.append(("", ins.ext, ins.cfg))
        
        template = Path("theme") / (ins.meta.get("theme") or ["post"]).pop()

        print("template", template)
        with template.open() as f:
            template = f.read()
        
        meta = ins.meta

        for k,v in meta.items():
            meta[k] = None if len(v) == 0 else v.pop()


        
        out = render(template, 
                {
                    "body": ins.txt, 
                    "slugs": slugs, 
                    "index": "index.html",
                    **ins.meta
                })

        templateds.append((out, ins.ext, ins.cfg))



    for txt, ext, path_config in templateds:
        output_handler = output_mapping.get(io_mapping.get(ext, io_none), output_none)
        output_handler.output(path_config, txt)
   

@click.command("serve")
@click.argument("src", type=Path)
def serve(src):
    @route('/<path>')
    def index(path):
        return static_file(path, src)

    @route('/')
    def index():
        return static_file("index.html", src)

    run(host='localhost', port=8080)


@click.command("graph")
@click.argument("src", type=Path)
def graph(src):
    wikilink_re = r'\[\[([\w0-9/_ -]+)\]\]'

    srcs = filter(lambda x : ".md" in str(x),  get_source_files(src))
    srcs = list(srcs)
    
    G = nx.DiGraph()

    for src in srcs:
        G.add_node(src.stem)

    for src in srcs:
        with open(src) as f:
            content = f.read()

        matches = re.findall(wikilink_re, content)
        for match in matches:
            print(match)
            G.add_edge(src.stem, match)
    
    UG = G.to_undirected()

    #print(G.nodes())
    clusters = list(nx.connected_components(UG))
    print("clusters", clusters)

    #print(nx.is_forest(G))
    #print(nx.maximum_spanning_tree(UG))
    #for node in G.nodes():
        #print(G[node].adj)
    #nx.draw(G, with_labels=True, font_weight='bold')
    nx.draw(G)
    plt.show()


@click.command("query")
@click.argument("src", type=Path)
@click.argument("query")
def query(src, query):
    srcs = get_source_files(src)

    for src in srcs:
        if not "md" in str(src):
            continue
        with open(src) as f:
            lines = f.readlines()

        for line in lines:
            if query in line:
                print(line)

        




@click.group()
def main():
    pass

main.add_command(convert)
main.add_command(serve)
main.add_command(query)
main.add_command(graph)

if __name__ == "__main__":
    main()

