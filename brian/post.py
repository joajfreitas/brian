from pathlib import Path

class Input():
    def __init__(self, ins, cfg):
        self.cfg = cfg
        self.ext = cfg.input.suffixes[-1][1:]
        self.txt, self.meta = ins
        self.path = self.cfg.input.relative_to(self.cfg.src)
        self.slug = Path(*self.path.parts[:-1]) / self.path.stem

